import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-ex3',
  templateUrl: './directives-ex3.component.html',
  styles: [`
    .logs {
      color: white;
    }
  `]
})
export class DirectivesEx3Component implements OnInit {
  display = false;
  click = [];
  cout = 0;
  isLog = false;
  properLength = false;

  constructor() { }

  ngOnInit() {
  }

  onButtonClick() {
    this.display = !this.display;
    this.cout += 1;
    this.click.push(this.cout + '. Button clicked.');
    // if (this.click.length >= 5) {
    //   this.isLog = true;
    // }
  }

}
