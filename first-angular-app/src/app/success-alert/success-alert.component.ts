import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-alert',
  templateUrl: './success-alert.component.html',
  styleUrls: ['./success-alert.component.css']
  // or
  // template: This is warning
  // styles: [
  // p {
  //   border: 1px solid red;
  //   padding: 20px;
  //   background-color: mistyrose;
  // }
  // ]
})
export class SuccessAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
