import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers', // normal naming - most used
  // selector: '[app-servers]', // naming as atribute for tag
  // selector: '.app-servers', // ass an class name -
  templateUrl: './servers.component.html'
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No server was created';
  serverName = 'Test server';
  serverCreate = false;
  servers = ['Test server 1', 'Test server 2'];

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
    // setInterval(() => {
    //   this.allowNewServer = !this.allowNewServer;
    // }, 2000);
  }

  ngOnInit() {
  }

  onCreateServer() {
    this.serverCreate = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server was created. The server name is ' + this.serverName ;
  }

  onUpdateServerName = (event: Event) => {
    this.serverName = (<HTMLInputElement> event.target).value;
    // info o tym w txt
  }

  // onServerChange() {
  //   if (this.serverName.length > 0) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}
