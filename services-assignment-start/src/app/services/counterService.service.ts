import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CounterService {
    incrementInactiveToActive = 0;
    incrementActiveToInactive = 0;

    inactiveToActive() {
        this.incrementInactiveToActive++;
        console.log('Inactive to Active: ' + this.incrementInactiveToActive);
    }

    activeToInactive() {
        this.incrementActiveToInactive++;
        console.log('Active to Inactive: ' + this.incrementActiveToInactive);
    }
}
