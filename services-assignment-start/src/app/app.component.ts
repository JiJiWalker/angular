import { Component, OnInit } from '@angular/core';
import { UserService } from './services/userService.service';
import { CounterService } from './services/counterService.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private counterService: CounterService) {}


}
