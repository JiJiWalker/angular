import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user: {id: number, name: string};
  paramsSubscription: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };
    // powyższe nie działa asynchronicznie jeżeli zmienią się paramsy będąc na tej samej stronie
    // do działania przy takiej zmianie potrzebujemy Observables
    this.paramsSubscription = this.route.params
      .subscribe(
        (param: Params) => {
          this.user.id = param['id'];
          this.user.name = param['name'];
        }
      );
  }

  ngOnDestroy() {
    // przestajemy czekać na event w momencie, kiedy przestaną istnieć te parametry (w momencie np. zmiany strony gdzie link będzie inny)
    // nie jest to wymagane bo angular sam to ogarnia
    this.paramsSubscription.unsubscribe();
  }

}
