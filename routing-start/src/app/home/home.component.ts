import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServersService } from '../servers/servers.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  onLoadServer(id: number) {
    this.router.navigate(['/servers', id, 'edit'], {queryParams: {allowEdit: '1'}, fragment: 'loading'});
  }

  onPromise() {
    const isMomHappy = true;

    const willGetNewPhone = new Promise(
      (resolve, reject) => {
        if (isMomHappy) {
          const phone = {
            brand: 'Samsung',
            color: 'black'
          };
          resolve(phone);
        } else {
          const reason = new Error('Mom is not happy');
          reject(reason);
        }
      }
    );

    const showOff = function (phone) {
      return new Promise(
          function (resolve, reject) {
              const message = 'Hey friend, I have a new ' +
                  phone.color + ' ' + phone.brand + ' phone';
              resolve(message);
          }
      );
    };
    // or shorten
    // const showOff = function (phone) {
    //   const message = 'Hey friend, I have a new ' +
    //               phone.color + ' ' + phone.brand + ' phone';
    //   return Promise.resolve(message);
    // };

    const askMom = function() {
      willGetNewPhone
        .then(showOff) // chaining
        .then(fulfilled => { console.log(fulfilled);
        })
        .catch(error => { console.log(error.message);
        });
    };

    askMom();
  }

  onLogin() {
    this.authService.login();
  }

  onLogout() {
    this.authService.logout();
  }

}
