import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  reference;
  @Output() timer = new EventEmitter<number>();
  num = 0;

  constructor() { }

  ngOnInit() {
  }

  onGameStart() {
    this.reference = setInterval(() => {
      this.timer.emit(this.num++);
    } , 1000);
  }

  onGameStop() {
    clearInterval(this.reference);
  }

}
