import { Component } from '@angular/core';
import { LoggingService } from '../shared/logging.service';
import { AccountsService } from '../shared/accounts.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
  // providers: [LoggingService] // no needed while @Injectable exist, but add it with no same services in parent (multi instance)
})
export class NewAccountComponent {

  constructor(private logginService: LoggingService, private accountService: AccountsService) {
    this.accountService.statusUpdate.subscribe(
      (status: string) => alert('New status: ' + status)
    );
    // this.accountService.statusUpdate.subscribe(status, function() {
    //   alert('asd');
    // });
  }

  onCreateAccount(accountName: string, accountStatus: string) {
    this.accountService.addAccount(accountName, accountStatus);
    // this.logginService.logStatusChange(accountStatus); // no needed more when service Log is injected in Account service
  }
}
