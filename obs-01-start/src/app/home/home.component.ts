import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription, Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  private firstObsSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    // this.firstObsSubscription = interval(1000).subscribe(count => { // przypisujemy suba do zmiennej, subskrybujemy
    //   console.log(count);
    // });
    // const customIntervalObservable = Observable.create(observer => {
    //   let count = 0;
    //   setInterval( () => {
    //     observer.next(count);
    //     count++;
    //   }, 1000);
    // });

    const customIntervalObservable = new Observable(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(count); // handle data
        if (count === 2) {
          observer.complete(); // handle completion
        }
        if (count > 3) {
          observer.error(new Error('Count is greater than 3!')); // handle error
        }
        count++;
      }, 1000);
    });

    // customIntervalObservable.pipe(map((data: number) => {
    //   return 'Round: ' + (data + 1);
    // }));
    // samo w sobie nie operuje na tym samym parametrze co powyższe Observable


    this.firstObsSubscription = customIntervalObservable.pipe(
      filter(data => {
        return data > 0;
      }),
      map((data: number) => {
        return 'Round: ' + (data + 1);
    })).subscribe(
      data => { // data param
        console.log(data);
      },
      error => { // error handle param
        console.log(error);
        alert(error.message);
      },
      () => { // completion handle function
        console.log('Completed');
      });
  }

  ngOnDestroy() {
    // this.firstObsSubscription.unsubscribe(); // unsubskrybujemy w momencie kiedy wyjdziemy z tego komponentu
    this.firstObsSubscription.unsubscribe();
  }

}
