import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  activated = false;
  private activeSubsribtion: Subscription;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.activeSubsribtion = this.userService.activated.subscribe(
      (isActivated: boolean) => {
        this.activated = isActivated;
      }
    );
  }

  ngOnDestroy() {
    this.activeSubsribtion.unsubscribe();
  }
}
