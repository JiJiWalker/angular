import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: any): any {
    const splitedString = value;
    const reversedString = splitedString.split('').reverse();
    return reversedString.join('');
  }

}
