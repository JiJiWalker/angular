import { Directive, OnInit, ElementRef, Renderer2, HostListener, Input, TemplateRef, ViewContainerRef, HostBinding } from '@angular/core';

@Directive({
  selector: '[appMyDropdown]'
})
export class MyDropdownDirective {
  @HostBinding('class.open') isOpen = false; // jeżeli false odejmuje class open, jeżeli true dodaje

  @HostListener('click') dropClick() {
    this.isOpen = !this.isOpen;
  }

  // @HostListener('document:click', ['$event']) dropClick(event: Event) { // zamyka dropdown przy kliknięciu gdziekolwiek na stronie
  //   console.log(event.target);
  //   this.isOpen = this.elRef.nativeElement.contains(event.target) ? !this.isOpen : false; // sprawdza czy el na stronie zawiera target
  // }

  // constructor(private elRef: ElementRef) {}

}
