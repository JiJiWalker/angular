export class Ingredient {
    // mały shortcut jak tworzyć klasę modelową/constructor.
    constructor(public name: string, public amount: number) {}
}
