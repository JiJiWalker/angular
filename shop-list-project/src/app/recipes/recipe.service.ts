import { Recipe } from './recipe.model';
import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable({
    providedIn: 'root'
})
export class RecipeService {
    recipes: Recipe[] = [
        new Recipe(
            'Chicken with fries',
            'Best meal',
            'https://www.wikihow.com/images/d/d7/Carve-a-Chicken-Step-13.jpg',
            [
                new Ingredient('Chicken', 1),
                new Ingredient('Fries', 30),
            ]),
        new Recipe(
            'Tortilla',
            'El picante',
            'https://recipeland.com/images/r/11561/7270f50ba2c8dd94b069_1024.jpg',
            [
                new Ingredient('Tortilla', 2),
                new Ingredient('Bean', 2),
                new Ingredient('Meat', 1),
            ]),
        new Recipe(
            'Pizza',
            'For party',
            'https://www.sickchirpse.com/wp-content/uploads/2016/01/pizza-stock.jpg',
            [
                new Ingredient('Cake', 1),
                new Ingredient('Corn', 2),
                new Ingredient('Meat', 1),
            ])
    ];


    constructor(private shoppingListService: ShoppingListService) {}

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(id: number) {
      return this.recipes[id];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        // using sl service with ingredients array
        this.shoppingListService.addIngredients(ingredients);
    }
}
