import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { ShoppingListService } from 'src/app/shopping-list/shopping-list.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  id: number;

  constructor(private recipeService: RecipeService, private router: Router, private routes: ActivatedRoute) { }

  ngOnInit() {
    this.routes.params
      .subscribe(
        (param: Params) => {
          // this.id = +param['id']; // also could be this way
          this.id = +param.id;
          this.recipe = this.recipeService.getRecipe(this.id);
        }
      );
  }

  onAddShoppingList() {
    // method from recipe service. Add ingredients list on (click) event
    this.recipeService.addIngredientsToShoppingList(this.recipe.ingredients);
    // poniższe również działa (w kursie było uwzględnione pokazanie jak działa cross service and cross component)
    // this.slServ.addIngredients(this.recipe.ingredients);
  }

  onEditRecipe() {
    this.router.navigate(['edit'], {relativeTo: this.routes});
    // this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.routes}); // można i tak za pośrednictwem przekazanej zmiennej
  }

}
