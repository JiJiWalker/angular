import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import {map, filter} from 'rxjs/operators';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  id: number;
  observ$: Subscription;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params.id;
    });

    const custObs = new Observable(observer => {
      observer.next(1);
      observer.next(2);
      observer.next(3);
      setTimeout(() => {
        observer.next(4);
        observer.complete();
      }, 1000);
    });

    this.observ$ = custObs.pipe(
      filter(data => data > 0),
      map(data => 'Nums: ' + data))
      .subscribe(output => {
      console.log(output);
    }, error => {
      console.log(error);
    }, () => {
      console.log('Completed');
    });
  }

  ngOnDestroy() {
    this.observ$.unsubscribe();
  }

  onActivate() {
    this.userService.activatedEmmiter.next(true);
  }
}
