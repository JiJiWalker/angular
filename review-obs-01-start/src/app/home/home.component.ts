import { Component, OnInit, OnDestroy } from '@angular/core';

import {interval, Observable, Subscription} from 'rxjs';
import {map, filter} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private interval$: Subscription;

  constructor() { }

  ngOnInit() {
    // this.interval$ = interval(1000).subscribe(count => {
    //   console.log(count);
    // });
    const customIntervalObservable = new Observable(observer => { // tutaj jest tworzony observable;
      let count = 0;
      setInterval(() => {
        observer.next(count);
        if (count === 2) {
          observer.complete(); // no argument
        }
        if (count > 3) {
          observer.error(new Error('COunt is greater than 3!'));
        }
        count++;
      }, 1000);
    });

    // customIntervalObservable.pipe(map((data: number) => {
    //   return 'Round ' + (data + 1);
    // })); <-- to nie działa standalone.

    this.interval$ = customIntervalObservable.pipe(filter(data => {
      return data > 0;
    }), map((data: number) => {
      return 'Round: ' + (data + 1);
    })).subscribe(data => { // tutaj musi zostać wywołany
      console.log(data);
    }, error => {
      console.log(error);
      alert(error.message);
    }, () => { // completed - no argument
      console.log('Completed');
    });
  }

  ngOnDestroy() {
    // this.interval$.unsubscribe();
    this.interval$.unsubscribe();
  }

}
