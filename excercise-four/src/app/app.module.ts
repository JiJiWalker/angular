import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GameCotrnolComponent } from './game-cotrnol/game-cotrnol.component';
import { OddComponent } from './odd/odd.component';
import { EvenComponent } from './even/even.component';
import { FibonacciNumberComponent } from './fibonacci-number/fibonacci-number.component';
import { FibonacciPrintComponent } from './fibonacci-print/fibonacci-print.component';

@NgModule({
  declarations: [
    AppComponent,
    GameCotrnolComponent,
    OddComponent,
    EvenComponent,
    FibonacciNumberComponent,
    FibonacciPrintComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
