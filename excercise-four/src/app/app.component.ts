import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  oddNumbers: number[] = [];
  evenNumber: number[] = [];
  fibNumber: number[] = [];

  ngOnInit() {
  }

  onIntervalFired(firedNumber: number) {
    if (firedNumber % 2 === 0) {
      this.evenNumber.push(firedNumber);
    } else {
      this.oddNumbers.push(firedNumber);
    }
  }

  onFiboAdded(fibNumber: number) {
    if (this.fibNumber.length === 0) {
      this.fibNumber.push(0);
      this.fibNumber.push(1);
    }
    this.fibNumber.push(fibNumber);
  }
}
