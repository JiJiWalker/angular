import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-fibonacci-number',
  templateUrl: './fibonacci-number.component.html',
  styleUrls: ['./fibonacci-number.component.css']
})
export class FibonacciNumberComponent implements OnInit {
  @Output() fNum = new EventEmitter<number>();
  @ViewChild('fibonContent', {static: true}) fibonContentInput: ElementRef;
  i: number;
  len = 10;
  num1 = 0;
  num2 = 1;
  num3: number;

  constructor() { }

  ngOnInit() {
    console.log('Fibb ' + this.num1);
    console.log('Fibb ' + this.num2);
  }

  onFibonacci() {
    for (let index = 2; index < (Number(this.fibonContentInput.nativeElement.value) + 2); index++) {
      console.log('Len: ' + (Number(this.fibonContentInput.nativeElement.value) + 2));
      this.num3 = this.num2 + this.num1;
      this.fNum.emit(this.num3);
      console.log('Fibb: ' + this.num3);
      this.num1 = this.num2;
      this.num2 = this.num3;
    }
  }

}
