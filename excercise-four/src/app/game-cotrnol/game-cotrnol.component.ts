import { Component, OnInit, DoCheck, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-game-cotrnol',
  templateUrl: './game-cotrnol.component.html',
  styleUrls: ['./game-cotrnol.component.css']
})
export class GameCotrnolComponent implements OnInit {
  @Output() intervalFired = new EventEmitter<number>(); // pojedyńczy parametr więc tylko number
  @Output() fibonacciNum = new EventEmitter<number>();
  @ViewChild('fiboInput', {static: true}) fiboEntry: ElementRef;
  count = 0;
  counter: any;
  fibNum1 = 0;
  fibNum2 = 1;
  fibNum3: number;
  isFiboEmpty = true;

  constructor() { }

  ngOnInit() {
  }

  onStart() {
    this.counter = setInterval(() => {
      // tutaj emitujemy event
      this.intervalFired.emit(this.count + 1);
      this.count++;
      console.log(this.count);
    }, 1000);
  }

  onStop() {
    clearInterval(this.counter);
  }

  onFibonacciNumberStart() {
    if (this.fiboEntry.nativeElement.value !== '') {
      this.isFiboEmpty = true;
      console.log((Number(this.fiboEntry.nativeElement.value) + 2));
      for (let index = 2; index < (Number(this.fiboEntry.nativeElement.value) + 2); index++) {
        this.fibNum3 = this.fibNum2 + this.fibNum1;
        this.fibonacciNum.emit(this.fibNum3);
        console.log(this.fibNum3);
        this.fibNum1 = this.fibNum2;
        this.fibNum2 = this.fibNum3;
      }
    } else {
      this.isFiboEmpty = false;
    }
  }

}
