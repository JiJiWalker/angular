import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fibonacci-print',
  templateUrl: './fibonacci-print.component.html',
  styleUrls: ['./fibonacci-print.component.css']
})
export class FibonacciPrintComponent implements OnInit {
  @Input() fibNum: number;

  constructor() { }

  ngOnInit() {
  }

}
