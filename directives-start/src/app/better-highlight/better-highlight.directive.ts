import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {
  @Input() defaultColor = 'transparent';
  @Input() customColor = 'red';

  constructor(private elRef: ElementRef, private render: Renderer2) { }
  @HostBinding('style.backgroundColor') backColor = this.defaultColor; // w par class.element do ostylowania, trzeba zainicjować default val

  ngOnInit() {
    this.backColor = this.defaultColor;
    // this.render.setStyle(this.elRef.nativeElement, 'background-color', 'red'); // 1 element do zmiany, 2. styl 3. wartość
  }

  @HostListener('mouseenter') mouseOn() {
    // this.render.setStyle(this.elRef.nativeElement, 'background-color', 'red'); // 1 element do zmiany, 2. styl 3. wartość
    this.backColor = this.customColor;
  }

  @HostListener('mouseleave') mouseOff() {
    // this.render.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
    this.backColor = this.defaultColor;
  }

}
