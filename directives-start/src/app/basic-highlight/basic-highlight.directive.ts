import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[appBasicHighLight]'
})
export class BasicHighLightDirective implements OnInit {
    constructor(private elRef: ElementRef<HTMLElement>) { } // injection element w miejscu gdzie directive zostanie użyty;
                                                            // został dodany generic type (<HTMLElement>), żeby w onOnInit()
                                                            // kompilator wiedział jakie są dostępne wartości

    ngOnInit() {
        this.elRef.nativeElement.style.backgroundColor = 'blue';
    }
}
