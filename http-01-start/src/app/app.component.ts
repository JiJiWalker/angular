import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Post } from './post.model';
import { PostService } from './posts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  loadedPosts: Post[] = [];
  isFetching = false;
  error = null;
  private errorSub: Subscription;

  constructor(private http: HttpClient, private post: PostService) {}

  ngOnInit() {
    this.setVariablesAndFetchPosts();
  }

  onCreatePost(postData: Post) {
    // Send Http request
    this.post.createAndStorePost(postData); // passing function as argument
  }

  onFetchPosts() {
    // Send Http request
    this.fetchPosts();
  }

  onClearPosts() {
    // Send Http request
    // this.post.deletePosts().subscribe(response => {
    //   console.log(response);
    // });
    this.post.deletePosts().subscribe(() => {
      this.fetchPosts();
    });
  }

  private fetchPosts() {
    this.setVariablesAndFetchPosts();
  }

  private setVariablesAndFetchPosts() {
    // W wypadku error wysyłamy error.msg z serwisu a tutaj tego nasłuchujey
    // przypisujemy do zmiennej bo trzeba unsubscribe()
    this.errorSub = this.post.error.subscribe(error => {
      this.error = error;
    });

    this.isFetching = true;
    this.post.fetchPosts().subscribe(
      posts => {
        console.log('Posts', posts);
        this.isFetching = false;
        this.loadedPosts = posts;
      },
      (error: Error) => {
        this.isFetching = false;
        this.error = error.message;
      }
    );
  }

  onHandleError() {
    this.error = null;
  }

  ngOnDestroy() {
    this.errorSub.unsubscribe();
  }
}
