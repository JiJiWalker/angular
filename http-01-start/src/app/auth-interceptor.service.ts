import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';

export class AuthInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const modifiedUrl = req.clone({
      // modyfikujemy URL
      headers: req.headers.append('Auth', 'xyz') // dodajemy swoje headers
    });
    return next.handle(modifiedUrl);
  }
}
