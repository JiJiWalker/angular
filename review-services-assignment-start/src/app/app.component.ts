import { Component, OnInit } from '@angular/core';
import { UserServiceService } from './shared/user-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
