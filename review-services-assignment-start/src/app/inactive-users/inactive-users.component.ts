import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { UserServiceService } from '../shared/user-service.service';
import { CounterServiceService } from '../shared/counter-service.service';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent implements OnInit {
  users: string[];
  count = 0;

  constructor(private userService: UserServiceService, private countService: CounterServiceService) {}

  ngOnInit() {
    this.users = this.userService.inactiveUsers;
    this.count = this.countService.activeToInactive;
  }

  onSetToActive(id: number) {
    // this.userSetToActive.emit(id);
    this.userService.addActiveUser(id);
  }
}
