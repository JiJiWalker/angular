import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { UserServiceService } from '../shared/user-service.service';
import { CounterServiceService } from '../shared/counter-service.service';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent implements OnInit {
  users: string[];
  count: number;

  constructor(private userService: UserServiceService, private countService: CounterServiceService) {}

  ngOnInit() {
    this.users = this.userService.activeUsers;
    this.count = this.countService.inactiveToActive;
  }

  onSetToInactive(id: number) {
    // this.userSetToInactive.emit(id);
    this.userService.addInactiveUser(id);
  }
}
