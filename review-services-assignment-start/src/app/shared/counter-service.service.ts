import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterServiceService {
  activeToInactive = 0;
  inactiveToActive = 0;

  constructor() { }

  incrActiveToInactive() {
    this.activeToInactive++;
    console.log('A: ', this.activeToInactive);
  }

  incrInactiveToActive() {
    this.inactiveToActive++;
    console.log('I: ', this.inactiveToActive);
  }
}
