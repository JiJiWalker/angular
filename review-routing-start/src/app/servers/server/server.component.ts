import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: {id: number, name: string, status: string};
  id: number;

  constructor(private serversService: ServersService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.data
      .subscribe(
        (data: Data) => {
          this.server = data['server'];
        }
      );
    // możemy to zakomentować gdyż nie potrzebujemy więcej przypisywać na init id, ponieważ korzystamy z resolve w routing
    // this.id = +this.route.snapshot.params['id'];
    // this.server = this.serversService.getServer(this.id);
    // this.route.params.subscribe(
    //   (params: Params) => {
    //      this.server = this.serversService.getServer(+params['id']);
    //   }
    // );
  }

  onEdit() {
    // this.router.navigate(['/servers', this.id, 'edit']);
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'}); // znajdujemy się w servers/1...
    // {path: ':id/edit', component: EditServerComponent}
  }

}
