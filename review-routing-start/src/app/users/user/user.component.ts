import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: {id: number, name: string};

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };
    console.log(this.route.snapshot);
    this.route.params // params is observable, co znaczy że nasłuchuje na zmianę, ale nie wiemy czy w ogóle i kiedy (asynchronymus task)
      .subscribe(
        (params: Params) => {
          console.log(params);
          this.user.id = params['id'],
          this.user.name = params['name'];
        }
      );
  }

}
