Node Starting:
    - npm install -g npm (instaluje npm)
    - npm cache clean --force (od wersji npm 5 potrzebujemy force)
    - npm install -g @angular/cli (instaluje command line interface)

Angular new project
    - (W docelowym folderze) ng new my-angular-app (tworzy folder i projekt my-angular-app)
    - ng serve (startuje serwer) asd
    - w folderze projektu: npm install --save bootstrap (bootstrap@3 jeżeli chcemy wersję 3, bez małpy aktualna wersja)
    - jeżeli nie działają podpowiedzi - npm install tslint w projekcie

Dodawanie projektu (w projekcie któy np został skopiowany)
    - npm install
    - npm uninstall -g angular-cli @angular/cli
    - npm cache clean --force
    - npm install -g @angular/cli

Good practice/tips:
    - naming files with pre folder name, than ex. component: server.component.ts/js
    - decorators - coś co rozszczerza klasę/elementy (feature typescript). Poprzedza go znak @
    - tworzenie nowego componentu kroki:
        - nowy folder + plik html oraz plik ts;
        - w pliku ts import component from @angular/core
        - @Component({}) który zawiera selector, templateUrl oraz styleUrl
        - export class NazwaKlasy;
        - constructor()
        - zaimportowanie oraz dodawanie w declarations nazwy klasy w app module
    - tworzenie nowego componentu:
        - manualnie klikająć i tworząc nowy folder/pliki
        - komendą 'ng generate component nazwaKomponentu (np serve)'
          bądź krótszym skrótem 'ng g c nazwaKomponentu' ( dodając na końcu --spec false pozbywamy się tworzenia pliku testowego)
    - tworzenie nowego directive:
        - ng g d nazwa-directive
    - tzw. backtick albo tempalte literals. Znak ` używany do "represent multi-line string and may use "interpolation" to inser variables
        (czyli var test2 = 12, test = `abc, 123
                                       cde 234
                                       f is ${test2}`)
    - arrow func: temp(() => {})
    - owijając parametr w html nawiasami kwadratowymi - [], np <button [disable]>Button</button>, udostępniamy możliwość dynamicznego sterowania tym parametrem
        <button [disable]="funkcjaSterującaZeSkryptu">Button</button> (funkcją sterującą bądź zmienną)
    - kiedy PROP BINDING, kiedy STRING INTERPOLATION:
        - STRING INTERPOLATION (<p>{{ zmienna }}</p>): kiedy chcemy coś zwrócić, pokazać
        - PROP BINDING (<p [innerText]="zmienna"></p>): kiedy chcemy zmienić property;
    - EVENT BINDING:
        - event dla button w angular przekazujemy jako parametr (w tagach, czyli as always), jako (click): <button (click)="funkcjaCosRobi()"></button>
          a nie onClick, onToggle, onHover.
    - $ EVENT BINDING
        - dla przykładu <input type="text" (input)="jakasFunkcja($event)">: w tym miejscu wychwytujemy wartość wpisaną w input i przekazujemy ją dynamicznie do zmiennej 
        w parametrze ($event)
    - HTML*: pobierając wartośc z DOM, możemy zrzutować typ np. HTMLInputElement do zdefiniowanego typem parametru (event: Event), jak event.target (<HTMLInputElement> event.target)
        dzięki czemu dostaniemy się do parametrów po target.
        - pierwotnie: event.target.value - źle (value nie istnieje bo jest specifide event: Event)
        - value jest wartością input'a, więc trzeba zrzutować (poinformować), że event.target jest inputem
        - rzutujemy HTMLInputElement w <> jako event.target, bierzemy z tego value (więcej o tym w txt)
    - TWO WAY BINDING:
        - działa tak, że np wartość zmiennej zmienia się zarówno tam jak i w input
        - w html dodajemy [(ngModel)]="zmienna" zamiast (input)="..."
    - Directives:
        - directives are instructions in the DOM (oznakowanie @Directives...)
        - są dodawane jak atrybuty (<p someDir>..</p>)
    *ngIf:
        - dodajemy z gwiązdką
        - structural directive (zmienia strukturę DOM, czyli dodaje albo nie dodaje)
        - w nawiasie może pojawić się zmienna/wartość/skrypt. Cokolwiek co zwraca true/false
    - ng-template:
        - komponent/tag który wskazuje miejsce w DOM do zmiany
        - jako parametr/marker tego miejsca przekazywany jest "marker" (wyżej) w postaci #jakisMarker, np <ng-template #jakisMarker><p></p></ng-template>
        - marker ten przekazywany jest do warunku *ngIf, np. <p *ngIf="zmiennaBool; else jakisMarker">Test</p>
    - ngStyle
        - attribute directives (zmienia element w któym zostanie umieszczony)
        - osadzamy w [], czyli [ngStyle]
        - nawiasy nie są częścią directive
        - oczekuje js object: {} kvp, jak [ngStyle]="{backgroundColor: metoda()}" (metoda, albo stałą zmienna)
    - constructor()
        - każda klasa go posiada
        - jest wywoływany raz w momencie tworzenia tego komponentu
    - z [] oraz bez []
        - bez: angular interpetuje to co jest w nawiasach jako String
        - z: angular próbuje to  evaluate (?) na stringa
        - czyli: name="Max", [name]="funkcjaZwracajaxaMax()"
    - [ngClass]
        - dynamicznie dodaje/usuwa/zmienia klasy CSS
        - przyjmuje js obiekt
        - also kvp, key to css name, value condition kiedy ta klasa powinno zostać dodana, np [ngClass]="{nazwaKlasyCSS: metoda() bądź zmienna === 'string' bądź zmienna == true}"
    - *ngFor:
        - also structural directive (stąd gwiazdka)
        - example: <someComponent *ngFor="let zmiennaTmp of zmiennaArrayZeSkryptu"></someComponent> (we want to duplicate this one)
        - czyli deklaracja zmiennej of lista po której będziemy iterować
    - tworzenie reprezentacji obiektu, czyli np. element: {type: string, name: string, content: string}
        - czyli element będzie typu js object
        - jest to ts syntax do definiowania typu, aby element miał tylko ten typ
        - jeżeli mamy np. listę obiektów lista1 w parencie, a obsługujemy tę listę w innym komponencie, to tworzymy taki typ (element), aby później wiedzieć, jakie dane 
          powinny się w nim zawierać (np. w pliku HTML przypisujemy [lista1]="lista2", czyli lista jako zdefiniowany typ chcemy żeby był lista2 z innego komponentu)
    - @Input('alias'): uwidacznia dany component/property na zewnątrz (PARENT -> CHILD)
        - musimy to również zaimportować
        - (RARE CASES) argument 'alias' przekazany w parametrze oznacza nazwę parametru jaką będziemy się posługiwać poza componentem. Np jak przekażemy 'testComp', to na zewnątrz
          będziemy się do tego odwoływali jako [testComp], a nie jako nazwa parametru w konponencie
    - @Output(): przekazuje event z child do parent (CHILD -> PARENT)
        - tworzymy custom event w komponencie (w child)
            - tworzymy 'proporties', czyli np. serverCreated;
            - tworzymy z properties event przypisująć zmienną: serverCreated = new EventEmmiter
            - EventEmmiter jest typem generycznym więc w <> podaje typ danych które chcemy dostać, tudzież np obiekt:
              serverCreated = new EventEmmiter<{serverName: string, serverContent: string}>;
            - dodajemy na końcu () żeby wywołać konstruktor i stworzyć nowy obiekt, czyli: serverCreated = new EventEmmiter<{serverName: string, serverContent: string}>()
        - w tym samym komponencie (przypisujemy co do czego), mając funckję która operuje na tych danych, wywołujemy ten parametr funkcją emit() w której przekazujemy zmienne z obiektu:
            onAddServer() {this.serverCreated.emit({serverName: this.innaZmienna, serverContent: this.innaZmienna})};
        - dodajemy znacznik @Output przed nasz event (stworzony parametr) dzięki czemu przekazujemy go wyżej:
            - @Output() serverCreated = new EventEmmiter...
        - przekazujemy nasz event do parenta np. w htmlu w tagach komponentu:
            - <app-comp (serverCreated)=""></app-comp>
        - w nawiasach przekazujemy metodę, która wykorzystuje zmienne siedzące w komponencie, w tym wypadku innaZmienna x2
            - <app-comp (serverCreated)="onDodajeDoStronyInnaZmienna()"></app-comp>
        - w parametrze metody przekazujemy $event dzięki czemu będziemy mogli złapać potrzebne nam dane:
            - <app-comp (serverCreated)="onDodajeDoStronyInnaZmienna($event)"></app-comp>
            (metoda onDodajeDoStronyInnaZmienna() w klasie parent wyglądała by tak): onDodajeDoStronyInnaZmienna(jakisParam: {npServName: string, npServCont: string}) {...}
    - encapsulation: zmienia zasady działania cssa w aplikacji
        - docelowo każdy komponent (łącznie z app.component) ma swój plik css który dla każdego jest traktowany oddzielnie
        - zmiana encapsulation (w komponencie>pliku .ts w znaczniku @Component, encapsulation: ViewEncapsulation.(...)) na np. none wyłącza tę zasadę i każdy css działą globalnie
        - rzadko używane
    - #local reference (starting by #)
        - marker
        - może być użyty w każdym elemencie w pliku HTML
        - lokalna referencja zwraca cały element w którym się znajduje, #inputElement w input zwróci cały input
        - możemy tą referencją operować w całym pliku html, ale nie poza nim (np. możemy przekazać jego wartość e metodzie która jest w template, który w pliku ts przyjmnie 
          wartość HTMLInputElement)
    - @ViewChild()
        - dostajemy się do lokalnej referencji elementu z pliku HTML w pliku ts
        - w parametrach podajemy nazwę referencji oraz {static: true};
        - deklarujemy parametr i dodajemy na początek dekorator: @ViewChild('referencjaHTML', {static: true}) jakisParametr;
        - jakisParametr po zalogowaniu jest zwracany jako typ ElementrRef, więc ...jakisParametr: ElementRef;
        - teraz możemy dostać się do wartości elementu z HTML poprzez this.jakisParametr.nativeElement.value;
    - ng-content (content projection)
        - https://www.udemy.com/the-complete-guide-to-angular-2/learn/v4/questions/4970350
        - działa jak projekcja. Dodajemy w template w komponencie HTML <ng-content></ng-content>
        - w parencie (app.component) w pliku html, odwołując się do tego komponentu po selektorze (np selektor komponentu był app-test-comp, i w app.component jest
          selector w HTML <app-test-comp>) pisząc coś w środku zastępujemy tag ng-content tym co dodaliśmy w aplikacji wyżej
    - Component lifecycle (zdarzenia, które Angular odpala w trakcie startowania)
        - ngOnChanges() - na starcie oraz w trakcie zmiany podane w parametrze komponentu=
        - ngOnInit() - podczas inicjalizacji (raz)
        - ngDoCheck() - może być wykonywany wieloktornie, przy każej zmianie
        - ngAfterContentInit() - wywoływany po tym, jak zostaje załadowany kontent (ng-content) do widoku
        - ngAfterContentChecked() - wywołwany za każdym razem kiedy powyższy kontent się zmieni
        - ngAfterViewInit() - wywoływany kiedy widoki komponentów i childów zostają zainicjalizowane
        - ngAfterViewChecked() - za każym razem kiedy widoku komponentów oraz childów "have been checked"
        - ngOnDestroy() - wywołwany raz w momencie kiedy komponent jest niszczony (np. ngIf)
    - $event
        - przekazujemy w parametrze metody w parencie do wartości emmit ((emittedVar)="appCompMethod($event)")
        - pozwala nam złapać wartość z childa
        - możemy też przekazać 'other-data', czyli precyzujemy typ zarówno w child jak i parent
        - dadatkowo $event możemy przekazać jako drugi argument
    - @Directive
        - oddzielny plik .ts
        - imporrtujemy Directive z @angular/core (import dir... powinno wczytać skrót)
        - jako param przyjmuje obiekt {selector: '[jakisSelector]'}
        - musi zostać zaimportowany do app.module 
        - budowa:
            import...
            @Directive({...});
            export class ...
            constructor() - tutaj przykład w basic-highlight-directive
            ngOnInit() - jak wyżej
            ...
    - Renderer2
        - better aproach to working with that and methot provided
        - dodajemy w constructor() w @Directive (constructor(elRef: ElementRef, render: Renderer2));
        - operujemy w ngOnInit()
    - @HostListener('event') jakasFunkcja() {}
        - dekorator;
        - nasłuchuje event podany w parametrze jako string, np. 'mouseenter', czyli najazd myszką na element
    - @HostBinding('class.element') jakasZmienna: typ = 'cos'
        - dekorator, również dobry aproach zamiast render i @HostListener
        - w parametrze przyjmuje classę oraz element do ostylowania (np. ElementRef: style.backgroundColo albo class.oddNum)
        - trzeba zainicjować defaultową wartość
        - można używać zamiast render i @HostListener
    - [ngIf]
        - używamy bez gwiazdki w ng-template
    - set
        - ustawiamy dla property w directive, np @Input() set jakasZmienna() {}
        - działa w wypadtku kiedy wynikiem jest false (odwrotność ngIf)
        - dzięki temu teoretycznie zmienia się to w metode, jednak nadal jest to property
        - kiedy property się zmienia (outside of directive), metoda jest wyzwalana, np. condition albo parametr dla tego property
        - nazwa zmiennej musi mieć taką samą nazwę co selector w directive
    - TemplateRef/ViewContainerRef
        - tak jak elementRef dale dostęp do elefemtnu, tak templateref daje dostęp do template w którym siedzi
        - w przypadku prcay w custom directive z templateRef, w constructorze podajemy to jako 1st param,
          jako drugi podajemy viewCOntainer, czyli np vcRef (template - what, vsRef - where)
    - [ngSwitch]
        - zwykły switch do któego przekazujemy zmienna
        - case dla switcha oznaczamy *ngSwitchCase
        - default oznaczamy jako *ngSwitchDefault
    - Service:
        Use Case:
        1. Reużywalne funckje
        2. Przetrzymywanie metod/tablic obiektów
        - dodajemy @Injectable({}) decorator na początku serwisu (jakisSerwis.service.ts)
        - w nawiasach dodajemy provideIn: 'root', dzięki czemu nie musimy dodawać provide: [] w dekoratorze @Component: w każdym komponencie który będzie używał serwisu**
        - w miejscu gdzie chcemy użyć serwisu w constructorze dodajemy 'constructor(private jakasZmienna: ServiceClass)', czyli typujemy parametr do typu serwisu;
        - po odwołaniu się do zmiennej private z constructora, dostajemy się do metod z serwisu
        - ** kwestia sporna. Czy dekorator ten używać always, czy tylko dla wild-app serwisów. Jeżeli to pierwsze, to co z providers: [], gdzie podajemy nazwy klas i z instancjami tych klas
          np. AppComp ma 1x provider TestServ, child Account oraz child Profil również mają po 1x taki provider. Razem mają 3x instancję, więc instancje z child nadpisuję instancje z AppComp
          a co za tym idzie jeżeli przekazywane są dane, dane są nadpisywane. W takim wypadku, tylko w elemencie najwyższym (w tym wyp. AppComp) powinien znajdować się dzielony service w provider: []
          w pozostałych natomiast powinien stamtąd zostać usunięty (ale tylko z providers: []. W constructor nadal powinien się znajdować)
        - możemy ustawić zmienną: EventEmmiter w serwisie, która będzie emitowana w jednym komponencie, a nasłuchiwana w drugim (poprzez subscribe())
        - przydatny jest emit jak pushujemy zmiany do listy, która jest zwracana z serwisu jako kopia (slice()). Wtedy emitujemy listę (również slice()) na metodzie click do serwisu z listą a tam subscrybujemy za każdym razem kiedy
          jest emit i przypisujemy listę z serwisu, do bierzącej listy.
    - ... (spread operator)
        - array of coś z dodanym na początku tym operatorem przekazuje/wyświetla listę nie jako object z X elementami, tylko przekazuje/wyświetla same elementy 
          (np. var arr = [1, 2, 3, 4, 5] console.log(arr) wyświetli tablicę do rozwinięcia z pięcioma indexami i elementami, > (5) [1, 2, 3, 4, 5])
          (console.log(...arr) wyświetli natomiast nie tablicę a same elementy, czyli 1, 2, 3, 4, 5)
    - Routing
        - możliwość wyświetlania podstron/zakładek na stronie z dodatkowym dopiskiem w URL (localhost:4200/home)
        - w app.module dodajemy zmienna const o typie Routes (const appRoutes: Routes);
        - przypisujemy do niej tablicę obiektów ( const appRoutes = Routes = [{ }]);
        - każdy obiekt przybiera dwa parametry odpowiadająca za to jaka będzie nazywać się ścieżka oraz jaki komponent ma wyświetlić:
          ({ path: 'users', component: NazwaComponentuDoWyświetlenia}, {...})
        - w app.module, w @NgModule, do tablicy importy dodajemy RouterModule
        - w metodzie RouterModule.forRoot() przekazujemy zmienną const appRoutes
        - w app.component, w miejscu gdzie mają się wyświetlać różne podstrony, zamiast selektorów w tagach dodajemy directive <router-outlet></router-outlet>
        - dodając do path 'sciezka/:id' ustawiamy dynamiczną zmienną po słowie ścieżka
    - routerLink
        - directive do przekazywania URL zamiast href, które odświeża nam stronę
        - np. routerLink="/home"
        - można też użyć property binding [routerLink]="'/home'" albo [routerLink]="['/home'], ['page']"
        - wartości bez '/' zostają dodane do aktualnego linka, wartości ze '/' kieruje bezpośrednio do ścieżki podanej, można też użyć '../' co cofnie o poziom wyżej i doda wartość
    - routerLinkActive
        - zakładki w nav (w bootstrap), na któych chcemy widzieć efekt że są kliknięte (aktywne) przybierają klasę 'active', ale nie jest ona dynamiczna
          w tym celu powstał directive routerLinkActive="active", który po kliknięciu zakładki z tym directive "ubiera" ją w taki wyglą.
        - w jej parametrze możemy podać swoją własną klasę css
        - dodajemy to nie w elemencie a, ale np. li
    - routerLinkActiveOptions
        - [routerLinkActiveOptions]="[exact: true]" - konfiguracja, działa tak, że kiedy tego nie ma, zakładka z pathem np, "/" jest ciągle jako aktywna, dlatego, że ten path występuje zawsze
          stąd directive powyższy, który mówi, że tylko wtedy kiedy path wygląda tak jak podany w routerLink, wtedy bądź active
    - router by button
        - możemy przekierować pod inny link wstrzykując router: Router do constructora, a później wywołując this.router.navigate(['/path']) 
        - metoda navigate nie ogarnia relative paths, więc żeby tak było, po argumencie 1st, dodajemy {relativeTo: this.route} (route wstrzykujemy jako route: ActivatedRoute)
    - [queryParams] oraz fragment:
        - aby nie stracić queryParam, które np. znajduje się w linku a po kliknięciu np buttona chcemy żeby było nadal bo określa jakieś zachowanie, do tablicy w metodzie navigate obok 
          relativeTo: this.route, dodajemy queryParamsHandling: 'preserve'. Dodaje istniejący query do linka. Można użyć również wartości 'merge' co merguje old and new
        routerLink approach
            - property które dodajemy obok routerLink.
            - [queryParams] przyjmuje js obiekt i kvp "{name: 'ala'}", "{color: 'blue'}" i dodaje to do linku ?name=ala
        programmaticaly approach (in ts file)
            - fragment przyjmuje string i dodaje #wartosc na końcu, fragment="wartosc"
            - w przypadku podejścia programatycznego z użyciem metody navigate, navigate(['/servers', id, 'edit]) <- id jest podane w metodzie jako param, więc  zmienia się dynamicznie
              dodatkowo można również podać queryParams oraz fragment: navigate(['/servers', id, 'edit], {queryParams: {allowEdit: '1'}, fragment: 'loading'})
    - route.snapshot a route.params
        - jeæeli chcemy pobrać przekazane dane w URL do componetu na init możemy wywołać metodę route.snapshot.params['nazwaIdentyfikatora']
        - nie działa to natomiast jęzeli jest on dynamiczny i zmienimy go w trakcie na tej samej stronie. Wtedy nam się nie odświeży
        - do tego celu używamy route.params.subscribe (route: Routes) => {}
    - multiply routes:
        - dodajemy w tym celu, jako trzeci param children: [{}], kóry przyjmuje listę param, które również mają ten sam początek, jak np server
        - usuwamy dla childów w path wtedy ten 'server' i zostawiamy reszte
        - dodatkowo, musimy dodać nowy router-outlet w komponencie, gdzie chcemy wyświetlić pozostałe komponenty
    - 404 redirecting
        - dodać component dla treście i ustawić path dla błędu
        - dodać niżej (na samym dole) path '**', i dodać redirectTo: '/nazwa-path'
        - teraz wszystko co nie będzie ustawione w app.module będzie odsyłało do komponentu z błędem
        - jeżęli chcemy przekierować path: ' ', musimy dodać pathMatch: 'full'. Wtedy przekieruje tylko wtedy jeżeli path nie zawiera nic więcej niż ' '
    - Custom AppModules
        - tworzymy plik *.module.ts
        - dodajemy to co potrzebujemy, dodajemy @NgModule, dodajemy imports
        - dodajemy exports gdzie przekazujemy samą klasę do której przypisaliśmy coś w imports
        - w app.module (głównym), w imports dodajemy klasę naszego customowego appcustommodule
    - Promise:
        - https://scotch.io/tutorials/javascript-promises-for-dummies
        - oraz przykład w home.component.ts w routing-start + auth-guard i auth service
    - AuthGuard:
        - szczegóły w lekcji 146 + routing-start app
        - aby przypisać takiego guarda do komponentu, w module (tam gdzie mamy routy ustawione) przypisujemy do parametru canActive: [NaszGuard]
        - aby przypisać do childów, impoekentujemy klasę CanActivateChild + metoda i używamy w module canActivateChild
    - CanDeactivate Guard:
        - lekcja 149;
        - implementacja interfejsu w klasie
        - can-deactivate-guard for reference in routing app
    - Passing data to route
        - static:
            - obok componentu w appmodule (tam gdzie trzymamy pathy), dodajemy property data: {message: 'wyswietl mnie'};
            - przekazuje kvp do komponentu który jest użyty w tej linii, z kolei wartość możemy złapać po wstrzyknięciu do konstruktora r: ActivatedRoute, 
              this.r.data.subscribe((data: Data) => zmienna = data['message']) albo po snapshot.param['message']
        - Dynamic:
            - potrzebujemy do tego intefrejsu Resolve, który zaciąga dane z 'backendu', przed np. załadowaniem się strony
            - przykład serwisu który używa tego interfejsu i działą jako resolver server-resolver w routing-project
            - w app routing dodajemy do odpowiedniego obiektu resolve: {customParam: NazwaComponentuZResolve}
            - od teraz dane któe zwracamy w serwisie z resolve będą dostępne w komponencie pod parametrem customParam
            - dostajemy się do tego tak samo jak do static, czyli this.route.data...
    14. Observable, operators, subscribe, Subjects, rx/js
    - Observables
        - różne eventy, hhtp req, trigger in code etc., które są wywoływane,
        - Observer z kolei jest to nasz kod, który wyzwala observables (handle data, handle error, handle completion)
        - pochodzą z paczki rxjs
        - interval from rxjs, przyjmuje parametr in milisec, który fire event co x milisecond, czyli np. interval(1000).subscrive(count => { console.log(count) });
          co sekunde dodaje 1 do count
        - jeżeli nie wywołamy unsubscribe na ondestroy, w tle cały czas będzie działać subscribe na funkcji interval * ilość wywołania (memory leak)
    - Custom Observable
        - importujemy Observable z rxjs
        - wywołujemy func create() (<- create jest deprecated, use new Observable instead); która przyjmuje funkcję 
        - funckja create(observer => {}) przyjmuje parametr, który rxjs dostarcza za nas
        - observer to "listener" któremy musimy powiedzieć, czy nasłuchujemy na dane, na error czy na complition (next() complete(), error())
        - observer.error() nie potrzebujy unscubscribe. W momencie wyrzucenia erroru przestaje nasłuchiwać automatycznie
        - aby go obsłyżyć, dodajemy parametr error w miejscu wywoływania Observable (this.asd - custObs.subscribe(data => {logic...}, error => {logic...});)
        - observer.complete() w momencie osiągnięcia warunku, zatrzymuje działanie subscribe()
        - aby go obsłyżyćm ddajemy trzeci parametr, który jest funkcją i nie przyjmuje żadnych argumentów( () => {}) i wstawiamy tam logikę (this.asd = custObs.subscribe(data => {logic...}, error => {logic...}, () => {logic...});)
        - custom są rzadko używane
    - operators
        - jeżeli chcemy robić operacje na danych z observable
        - używamy ich calling method pipe() (build into rxjs)
        - operatory są w rxjs/operators. Jednym z częściej używanych jest map() (przybiera funkcję jako arg, a jej argumentem jest data, które oryginalnie są w subscribe (home.components.ts obs project))
        - na tym przykłądzie z map zwracamy zmodyfikowany argument map
        - pozostałe operatory dołączamy przed map
        - operatorów musimy używać w miejscu gdzie subscrybujemy, czyli zmiennaObs.pipe(filter(() => {...}), map(() => {...})).subscribe(() => {...})
    - Subject
        - import from rxjs
        - zamiast new EventEmmiter<t>(), to new Subject<T>(); (Subject również jest generic type)
        - zamiast emit w wypadku Subject używamy next()
        - Subject jest special ind observable
        - more active than observables, może być ztriggerowane z kodu
    15. Handling Forms in Angular
    - Form Approaches
        - Tempate Driven (setup from in template - html, where Angular infers the FOrm Object from DOM)
        - Reactive Approach (we define structure of the form in ts code, html template, and connect both)
    - Template Driven
        - aby dodać element do js from representation (przez angulara), należy dodać parametr ngModel (bez [] oraz ()) oraz dodać i nazwać parametr name="jakasNazwa"
        - aby obsłużyć np. submit dodajemy (ngSubmit)="jakasFunkcja()" do elementu form. Dzięki temu zapobiegamy defaultowemu zachowaniu wysyłania req do servera (gdybyśmy dodali funkcje do buttona)
        - aby wyświetlić zawartość - referencja na <form #f>, gdzie f jest przekazane do metody jako HTMLFormElement
          nie mniej powyższe zwraca sama strukturę
        - aby powiedzieć Angularowi, żeby zwrócił ten from (który jest jakoby selektorem w tym wypadku) w postaci obiektu, przekazujemy do refenrecji #f="ngForm"
        - zmienia się również typ przyjmowanego obiektu w metodzie z HTMLFormElement na NgForm
        - można się również dostać poprzez @ViewChild('nazwaReferencji', {...}) jakaszmienna: NgForm; czyli przez console.log(this.jakasZmienna)
        - Valid(acja)
            - dodająć required na pole (lub/i build in validation) angular dynamicznie dodaje classy do oznaczone elementu i ustawia valid na poziomie obiektu bądź controls na false/true
            - operacja na klasach .ng-invalid oraz .ng-touched w celu np. borderu wokół input czerwonego (w momencie kliknięćia i nie wpisania niczego)
              klasy invalid występuje jak element z required jest puste, a touched jak zostanie kliknięte
            - aby wyświetlić np wiadomość walidacje pod konkretnym inputem możemy odwołać się do konkretnie tego elementu za pomocą referencji, np #email="ngModel" (nie ngForm jak dla formy)
            - dalej, w ngIf podajemy np. *ngIf="!email.valid && email.touched"
        - Binding
            - via [ngModel]="", np. w elementach options mamy value dla opcji, w zmiennych przypisujemy jakasZmienna = 'pet' i przekazujemy zmienną do nawiasów w [ngModel]
            - via [(ngModel)]="". 
            Summary: ngModel jako wskaźnik dla angulara, [ngModel] jako prop binding, [(ngModel)] jako two-way binding
        - Gruping form controls
            - np. mamy formularz z user data, klient1 data, klient2 data i chcemy pogrupować to co zwróci nam angular w postaci obiektu js.
            - do elementu (np div), który znajduje się w form oraz owrapowuje sekcję np. user, dodajemy ngModelGroup="jakasNazwa"
            - nazwa ta pokaże się w value oraz controls
            - tak samo jak poprzednio, możemy się również grupować wpisując referencje, np #userData="ngModelGroup" w div który wrappuje całość inputów/selectów itd
        - Radio buttons:
            - array with option (radioOption = ['female', 'male'])
            - loop in div (*ngFor="let gender of radioOptions")
            - input with radio type (type="radio" name="gender", ngModel (dlatego żeby dodało do obiektu) [value]="gender" (żeby value było opcjami z array) oraz między >{{ gender }}< żeby wyświetliło opcje)

        
GIT:
    - ctwl + enter: COMMIT
    - ctrl + alt + p: PUSH
    - git pull and then git push origin branchzktórego:branchdoktórego

Offtop:
    - prettier code with shift + alt + f
    - excencion: prettier
    - all settings in setting.json
    - if project use .editorconfing it override setting.json, so add this to this file (quote_type = single) and save

    1.
    map, mergeMap and switchMap will transform the passed value and return an Observable for the result.
    2.
    mergeMap and switchMap will additionally "flatten" an Observable of Observables to an Observable of normal values, by subscribing internally to the inner Observables.
    3.
    switchMap will additionally unsubscribe from previous inner Observables each time a new value of the outer Observable arrives.
    Watch this funny video, and you will never forget what switchMap does: → https://www.youtube.com/watch?v=rUZ9CjcaCEw
