import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'forms-td-final';
  @ViewChild('f', {static: false}) form: NgForm;
  subOption = '2';
  submitted = false;
  user = {
    email: '',
    subs: '',
    pass: ''
  };

  onSubmit() {
    console.log(this.form);
    this.submitted = true;
    // this.form.form.patchValue();
    this.user.email = this.form.value.email;
    this.user.subs = this.form.value.subs;
    this.user.pass = this.form.value.pass;
  }
}
